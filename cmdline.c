#include <assert.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>

#include "cmdline.h"

int
cmdline_usage (FILE *ferr, const char *prog_name,
	       struct cmdline_optdef *optdefs, size_t noptdef)
{
    assert (prog_name && optdefs);

    if (! ferr)
	ferr = stderr;

    /* Check for existence of options. */
    int has_opts = 0;
    int has_args = 0;
    int max_len = 0;
    size_t n;
    struct cmdline_optdef *optdef_p;
    for (n = noptdef, optdef_p = optdefs; n; n--, optdef_p++) {
	int len = 1;  /* pre-padding. */

	if (optdef_p->opt_short || optdef_p->opt_long) {
	    has_opts++;

	    /* Calculate length of usage line. */
	    len += 2;  /* '-s' */
	    if (optdef_p->opt_long) {
		len += 4 + strlen (optdef_p->opt_long);  /* ', --long' */
		if (optdef_p->has_arg)
		    len++;  /* '=' */
	    }
	    if (optdef_p->has_arg) {
		if (optdef_p->has_arg == 2)
		    len += 2;  /* '[' ']' */
		len += strlen (optdef_p->arg_name);  /* 'ARG' */
	    }
	} else if (optdef_p->help) {
	    has_args++;
	    len += strlen (optdef_p->arg_name);  /* 'ARG' */
	}

	len += 2; /* post-padding. */

	if (len > max_len)
	    max_len = len;
    }

    /* Print usage message. */
    char *prog_name_suffix = strrchr (prog_name, '/');
    fprintf (ferr, "Usage: %s%s",
	     (prog_name_suffix ? prog_name_suffix + 1 : prog_name),
	     (has_opts ? " [OPTION]..." : ""));
    for (n = noptdef, optdef_p = optdefs; n; n--, optdef_p++) {
	if (! (optdef_p->opt_short || optdef_p->opt_long)) {
	    fprintf (ferr, (optdef_p->has_arg == 0 || optdef_p->has_arg == 3 ?
			    " [%s]" : " %s"),
		     optdef_p->arg_name);
	    if (optdef_p->has_arg > 1) {
		fprintf (ferr, "...");
		break;
	    }
	}
    }
    fprintf (ferr, "\n");

    if (has_opts) {
	fprintf (ferr, "Options:\n");

	for (n = noptdef, optdef_p = optdefs; n; n--, optdef_p++) {
	    if (! (optdef_p->opt_short || optdef_p->opt_long))
		continue;

	    int len = fprintf (ferr, " ");

	    if (optdef_p->opt_short)
		len += fprintf (ferr, "-%c", optdef_p->opt_short);
	    else
		len += fprintf (ferr, "  ");

	    if (optdef_p->opt_long) {
		len += fprintf (ferr, optdef_p->opt_short ? ", " : "  ");
		len += fprintf (ferr, "--%s", optdef_p->opt_long);
	    }

	    if (optdef_p->has_arg)
		len += fprintf (ferr, (optdef_p->has_arg == 2 ? "[%s%s]" : "%s%s"),
				(optdef_p->opt_long ? "=" : ""),
				optdef_p->arg_name);

	    char *bol = optdef_p->help;
	    while (bol) {
		fprintf (ferr, "%*c", max_len - len, ' ');
		char *eol = strchr (bol, '\n');
		fprintf (ferr, "%.*s", (int) (eol ? eol - bol : strlen (bol)), bol);
		if ((bol = eol)) {
		    bol++;
		    len = 0;
		    fprintf (ferr, "\n");
		}
	    }
	    fprintf (ferr, "\n");
	}
    }

    if (has_args) {
	fprintf (ferr, "Arguments:\n");

	for (n = noptdef, optdef_p = optdefs; n; n--, optdef_p++) {
	    if (optdef_p->opt_short || optdef_p->opt_long || ! optdef_p->help)
		continue;

	    int len = fprintf (ferr, " ");
	    len += fprintf (ferr, "%s", optdef_p->arg_name);

	    char *bol = optdef_p->help;
	    while (bol) {
		fprintf (ferr, "%*c", max_len - len, ' ');
		char *eol = strchr (bol, '\n');
		fprintf (ferr, "%.*s", (int) (eol ? eol - bol : strlen (bol)), bol);
		if ((bol = eol)) {
		    bol++;
		    len = 0;
		    fprintf (ferr, "\n");
		}
	    }
	    fprintf (ferr, "\n");
	}
    }

    return 0;
}


int
cmdline_process (FILE *ferr, int argc, char **argv,
		 struct cmdline_optdef *optdefs,
		 size_t noptdef, void *cfg)
{
    int ret = 0;

    assert (argv && optdefs);

    if (! ferr)
	ferr = stderr;

    /* Populate option arrays. */
    char *opts_short = (char *) malloc (noptdef * 3 + 1);
    struct option *opts_long =
	(struct option *) malloc (noptdef * sizeof (struct option));
    char *short_p = opts_short;
    struct option *long_p = opts_long;

    struct cmdline_optdef *optdef_p = optdefs;
    size_t n = noptdef;
    for (optdef_p = optdefs; n; n--, optdef_p++) {
	if (! (optdef_p->opt_short || optdef_p->opt_long))
	    continue;

	if (optdef_p->opt_long) {
	    long_p->name = optdef_p->opt_long;
	    long_p->has_arg = optdef_p->has_arg;
	    long_p->flag = NULL;
	    long_p->val = (optdef_p->opt_short ? optdef_p->opt_short :
			   n - noptdef - 2);
	    long_p++;
	}

	if (optdef_p->opt_short) {
	    *short_p++ = optdef_p->opt_short;
	    switch (optdef_p->has_arg) {
	    case 2:
		*short_p++ = ':';
	    case 1:
		*short_p++ = ':';
	    }
	}
    }
    *short_p = '\0';

    /* Supress error logging to stderr. */
#if 0
    opterr = 0;
#endif

    /* Parse command-line options. */
    while (1) {
	int opt_idx = 0;
	int c = getopt_long (argc, argv, opts_short, opts_long, &opt_idx);

	if (c == -1)
	    break;
	else if (c == '?') {
	    cmdline_usage (ferr, argv[0], optdefs, noptdef);
	    ret = -1;
	} else {
	    if (c < 0)
		optdef_p = optdefs - (c + 2);
	    else
		for (n = noptdef, optdef_p = optdefs; n; n--, optdef_p++)
		    if (c == optdef_p->opt_short)
			break;

	    ret = optdef_p->handler (optarg, cfg);
	    if (ret == -2)
		cmdline_usage (ferr, argv[0], optdefs, noptdef);
	}

	if (ret)
	    break;
    }

    /* Get non-option argument(s). */
    optdef_p = optdefs;
    n = noptdef;
    while (! ret && optind < argc) {
	while (n && (optdef_p->opt_short || optdef_p->opt_long)) {
	    optdef_p++;
	    n--;
	}

	ret = (n ? optdef_p->handler (argv[optind], cfg) : -1);
	if (ret && opterr)
	    fprintf (ferr, "excess arguments\n");

	optind++;
	if (optdef_p->has_arg < 2) {
	    optdef_p++;
	    n--;
	}
    }

    free (opts_short);
    free (opts_long);

    return ret;
}



