#ifndef __CMDLINE_H
#define __CMDLINE_H

#include <stdlib.h>

struct cmdline_optdef {
    char opt_short;
    char *opt_long;
    int has_arg;
    char *arg_name;
    int (*handler) (char *, void *);
    char *help;
};

int cmdline_usage (FILE *, const char *, struct cmdline_optdef *, size_t);
int cmdline_process (FILE *, int, char **, struct cmdline_optdef *, size_t, void *);

#endif /* __CMDLINE_H */

