CFLAGS=-Wall -O2

.PHONY: all clean

all: conix

clean:
	rm -f conix *.o

conix: conix.o cmdline.o

conix.o: logo.h cmdline.h Makefile

cmdline.o: cmdline.h Makefile

